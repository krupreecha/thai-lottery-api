<?php


    $current_lotto_name_space = (isset($_REQUEST['s']))?$_REQUEST['s']:'';
    $lotto_day = (isset($_REQUEST['lotto_day']))?$_REQUEST['lotto_day']:'';
    $lotto_number = (isset($_REQUEST['lotto_number']))?$_REQUEST['lotto_number']:'';   
    
    // สำหรับเรียกรายการวันที่แสดงผลฉลากแต่ละงวด
    $server_url = 'https://www.krupreecha.com/api/lotteryday';
    $api_key = 'your api here';

    $header = array(
        "Content-Type: application/json",
        "Accept: application/json",
        "X-API-KEY:".$api_key,
        "domain:".$_SERVER["SERVER_NAME"]
    );
    $ch = curl_init($server_url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_POST, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
    $result = curl_exec($ch);
    curl_close($ch);
    $json = json_decode($result);
    $lotteryday = (isset($json->result))?$json->result:array();


    // สำหรับเรียกดูผลรางวัลแต่ละงวด
    $server_url = 'https://www.krupreecha.com/api/'.$current_lotto_name_space;
    $header = array(
        "Content-Type: application/json",
        "Accept: application/json",
        "X-API-KEY:".$api_key,
        "domain:".$_SERVER["SERVER_NAME"]
    );
    $ch = curl_init($server_url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_POST, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
    $result = curl_exec($ch);
    curl_close($ch);
    $json = json_decode($result);
    $lotto = (isset($json->result))?$json->result:array();

   
    // สำหรับตรวจว่าถูกรางวัลหรือไหม
    if(isset($lotto_day) && isset($lotto_number)){
        $server_url = 'https://www.krupreecha.com/api/verify/'.$lotto_day.'/'.$lotto_number;
        $header = array(
            "Content-Type: application/json",
            "Accept: application/json",
            "X-API-KEY:".$api_key,
            "domain:".$_SERVER["SERVER_NAME"]
        );
        $ch = curl_init($server_url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
        $result = curl_exec($ch);
        curl_close($ch);
        $winner = $result;
    }
    
    

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Example Thai Lottery API</title>
    
    <link href="https://fonts.googleapis.com/css2?family=Prompt&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">   
    <link id="theme-style" rel="stylesheet" href="assets/plugins/sweetalert/sweetalert.css">
    <link id="theme-style" rel="stylesheet" href="assets/css/styles.css">
</head> 


<body class="landing-page">   
    <div class="page-wrapper">
   
        
        <section class="cards-section text-center">
            <div class="container-fluid">
                <div class="row text-center">
                    <div class="col-lg-12 col-md-12 ">
                        <div class="card pr-3 pl-3">
                            <div class="row">
                                

                                <div class="col-md-4 col-xs-12 item item-green">
                                    <div class="item-inner">
                                        <div class="soc-header box-facebook text-left">
                                            <div class="form-group">
                                                <h2>ผลสลากย้อนหลัง</h2>
                                                <span class="text-muted">เลือกงวดที่ต้องการตรวจสลาก</span>
                                                
                                            </div>
                                            <div class="form-group">
                                                <form action="" method="get">
                                                    <select name="s" class="form-control custom-select" onchange="this.form.submit()">
                                                        <?php 
                                                            $last = 0;
                                                            foreach($lotteryday as $row){
                                                                $thaidate = $row->thaidate;
                                                                $lotto_name_space = $row->lotto_name_space;
                                                                if($last == 0){
                                                                    if($lotto_day == '')$lotto_day = $lotto_name_space;
                                                                    $last_string = "งวดล่าสุด";
                                                                }else{
                                                                    $last_string = "";
                                                                }
                                                                $last++;

                                                                if(!empty($current_lotto_name_space) && $current_lotto_name_space == $lotto_name_space){
                                                                    $lotto_day = $current_lotto_name_space;
                                                                    $current_thaidate = $thaidate;
                                                                    $selected = 'selected';
                                                                }else{
                                                                   
                                                                    $selected = '';
                                                                }

                                                               
                                                        ?>
                                                        <option value="<?php echo $lotto_name_space; ?>" <?=$selected; ?>><?php echo $thaidate; ?> <?=$last_string; ?></option>

                                                        <?php } ?>
                                                    </select>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-xs-12 item item-green">
                                    <div class="item-inner">
                                        <div class="soc-header box-facebook text-left">
                                            <h2>ผลสลากกินแบ่งรัฐบาล</h2>
                                            <h5>งวดประจำวันที่ <span class="text-muted"><?php if(isset($current_thaidate)) echo $current_thaidate;?></span></h5>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-xs-12 item item-green">
                                    <div class="item-inner">
                                        <div class="soc-header box-facebook text-left">
                                            
                                            <div class="form-group">
                                                <h2>ตรวจสลากอัตโนมัติ</h2>
                                                <span class="text-muted">ระบุเลขสลากที่ต้องการตรวจ 6 หลัก</span>
                                            </div>
                                            <div class="form-group">                        
                                                <form method="POST">
                                                    <div class="input-group">
                                                        <input name="lotto_day" type="hidden" value="<?php echo $lotto_day; ?>">
                                                        <input id="lotto_number" minlength="6" maxlength="6" class="form-control" name="lotto_number" type="search" placeholder="กรอกเลขสลาก" aria-label="ตรวจหวย" required>
                                                        <div class="input-group-append">
                                                            <button class="btn btn-info" type="submit">ตรวจหวย</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>


                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                <?php         
                foreach($lotto as $row){
                        $id = $row->id;
                        $name = $row->name;
                        $amount = $row->amount;
                        $reward = $row->reward;
                        $number = $row->number;
                ?>
                    <div class="col-12 p-3">
                            <div class="card">
                                <div class="row">
                                    <div class="col-12 item item-green">
                                        <div class="item-inner">
                                            <div class="soc-header box-facebook p-3">
                                                <h3 class="title"><?=$name; ?> มี <?=$amount; ?> รางวัล</h3>
                                                <span class="text-muted">รางวัลละ <?=number_format($reward,0); ?> บาท</span>
                                            </div>
                                            <div class="row soc-content ">
                                                <?php if(!is_array($number)){ ?>
                                                    <div class="col b-r text-center">
                                                        <h3 class="intro"><?php echo $number; ?></h5>
                                                    </div>
                                                <?php }else{ ?>
                                                    <?php for($i = 0;$i<count($number);$i++){ ?>
                                                    <div class="col b-r text-center">
                                                        <h3 class="intro"><?php echo $number[$i]; ?></h5>
                                                    </div>
                                                <?php }} ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>

                    <?php } ?>
                          
                      

                  

                </div>
            </div>


        </section>
    </div>


    <footer class="footer text-center">
        <div class="container">
            <small class="copyright">สงวนลิขสิทธิ โดย <a href="https://www.krupreecha.com" target="_blank">Thai Lottery API</a> for developers / Contact us krupreecha.api@gmail.com</small>
        </div>
    </footer>
    
          
    <script type="text/javascript" src="assets/plugins/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>  
    <script type="text/javascript" src="assets/plugins/sweetalert/sweetalert.min.js"></script>                                                              

    <script>
        var json = '<?php if(isset($winner)) echo $winner; ?>';
        var winner = '';
        
        if(json != ''){
            json = JSON.parse(json);
            console.log(json);
            winner = json.winner;
            if(winner){
                swal("ยินดีด้วย !",json.result[0].name);
            }else if(winner == false){
                swal("เสียใจด้วย !","คุณไม่ถูกรางวัล");
            }
        }
    </script>
</body>
</html> 

